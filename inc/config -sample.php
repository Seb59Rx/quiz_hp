<?php
//configuration du quiz
define('NB_QUESTION', 10);  //nombre de question par tirage
define('TO_WIN', 10);       //nombre de bonnes réponses pour gagner

define('MESSAGE_WIN', "Adressez vous a un membre du staff, sans fermer cette page.");
define('MESSAGE_LOOSE', "N'hésitez pas a retenter votre chance !!");

define('SQL_DSN',      'mysql:dbname=quizz;host=localhost');
define('SQL_USERNAME', 'admin');
define('SQL_PASSWORD', 'password');
