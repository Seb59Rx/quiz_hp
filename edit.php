<?php
require "inc/pdo.php";
require "inc/config.php";
require "inc/functions.php";

if (isset ($_POST["modifier"])) {
    //modifier la base
   
    update_base($_POST["qid"],$_POST["q"],$_POST["rid"],$_POST["r"]);

    //revenir a l'admin
    header('Location: admin.php'); 
    }
    
$question = getQuestionById($_GET['id']);
$reponse = getReponse($_GET['id']);

?>


<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="admin.css">
        <title>Admin Quizz Ludigeek</title>
    </head>
    <body>
        <div class ="container">
            
            <form method="post">
                <div>
                
                <input id="qid0" class="input" name="qid0" type="text"  disabled="disabled" value="<?= $question['id'] ?>" size="3" />
                <input id="qid" class="input" name="qid" type="hidden" value="<?= $question['id'] ?>" size="3" />
               
                <input id="q" class="input" name="q" type="text" value="<?= $question['question'] ?>" size="100" />
                <br>
                <input id="rid0" class="input" name="rid0" type="text" disabled="disabled" value="<?= $reponse['0']['id'] ?>" size="3" />
                <input id="rid" class="input" name="rid" type="hidden" value="<?= $reponse['0']['id'] ?>" size="3" />

                <input id="r" class="input" name="r" type="text" value="<?= $reponse['0']['reponse'] ?>" size="100" />
                <p>
                    <button type="submit" class="btn btn-primary" name="modifier" value="modifier">Submit</button>
                </p>
                </div>
            
            </form>
        </div>
    </body>
</html>
