<?php 
    require "inc/pdo.php";
    require "inc/config.php";
    require "inc/functions.php";

    //si le jeu n'est pas en status "RUN"
    if(!isset($_SESSION['run']) || (!$_SESSION['run'])){
        
        // Tirage des X questions
        $questions = getQuestions(NB_QUESTION); 
        
        // Raz du compteur
        $_SESSION['compteur'] = 0; 
        
        // Mise en memoire des questions
        for($i=0; $i < NB_QUESTION; $i++){ 
            $Q = "question_".$i;
            $_SESSION[$Q] = $questions[$i]['id'];
        }

        // Mise en memoire des reponses
        for($x=0; $x < NB_QUESTION; $x++){
            $question = $_SESSION["question_" . $x];
            $reponse = getReponse($question);
            $R = "good_".$x;
            $_SESSION[$R] = $reponse['0']['reponse'];
        }         
        

        // passage en RUN
        $_SESSION['run'] =true; 
    }  
    
    // Si on atteint le nombre de question max, on affiche les resultats
    if($_SESSION['compteur'] >= NB_QUESTION){
        header('location:resultats.php');
    }
?>


<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="styles.css">
        <title>Quizz Ludigeek</title>
    </head>
    <body>
        <div class ="container">
            <header class="ludigeek">
                Ludigeek - Quiz Harry Potter                
            </header>            
            <section>
                <div id="question" class="question"> 

                <?php 
                    $question = getQuestionById($_SESSION['question_'.$_SESSION['compteur']]); 
                    echo "<p>" . $question['question'] ."</p>
                          <p class =\"small muted\"> (id:" . $question['id'] . ")</p>";
                ?>                    
                </div> 
            </section>
            <section>
                <div id="reponses" class="reponses">
                    <ol>
                        <?php 
                        $reponse  = getReponseById($_SESSION['question_'.$_SESSION['compteur']]); 
                        $reponses = getReponses(3, $reponse['id_category'], $reponse['id']);

                        for($i=0; $i<3;$i++){ 
                            $reponses[$i]['reponse'] = $reponses[$i]['reponse'];
                        }
                        $reponses[3]['reponse'] = $reponse['reponse'];

                        shuffle($reponses);

                        for($i=0; $i<4;$i++){
                            echo "<li class=\"espace\">" . $reponses[$i]['reponse'] . "</li>";                  
                        }
                        ?>
                    </ol>
                </div>    
                    <form id="form" method="GET" action="check.php">               
                        <div class="boutons">
                            <button class="btn-carre btn-gry" type="submit" name="choix" value="<?= $reponses['0']['reponse']?>">Choix 1</button>
                            <button class="btn-carre btn-ser" type="submit" name="choix" value="<?= $reponses['1']['reponse']?>">Choix 2</button>
                            <button class="btn-carre btn-pou" type="submit" name="choix" value="<?= $reponses['2']['reponse']?>">Choix 3</button>
                            <button class="btn-carre btn-rav" type="submit" name="choix" value="<?= $reponses['3']['reponse']?>">Choix 4</button>
                        </div>    
                    </form>            
            </section>
            <section>
            <div class="bar">
                    <?php 
                        // Calcul du %
                        $en_cours = $_SESSION['compteur'];
                        $percent = ((( $en_cours / NB_QUESTION)*100)."%");
                    ?>
                    <div class="pourcentage"  style="width: <?= $percent ?>"></div>
                </div>
            </section>
        </div>  
    </body>
</html>