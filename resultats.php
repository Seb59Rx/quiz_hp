<?php 
    require "inc/pdo.php";
    require "inc/config.php";
    require "inc/functions.php";

    // Mise a l'arret du quiz
    $_SESSION['run']= 0; 
    $points = 0;
    $game = addGame();

    

    for($i=0; $i < NB_QUESTION;$i++){ 
        if($_SESSION['reponse_'.$i] === $_SESSION['good_'.$i]) {
            $points++;
        }
    }

    if($points >= TO_WIN) { 
        $status="Bravo, tu as fait un sans faute !!";
        $classe = "success";
        $win = addWin();
    } else {
        $erreurs = NB_QUESTION - $points;
        $erreurs > 1 ? $status = "Perdu :(  Vous avez fait " . $erreurs . " erreurs" : $status = "Perdu :(  Vous avez fait " . $erreurs . " erreur"; 
        $classe = "lose";
    }


?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="styles.css">
        <title>Quizz Ludigeek</title>
    </head>
    <body>
        <div class ="container">
            <header class="ludigeek">
                Ludigeek - Quiz Harry Potter                
            </header> 
            <<section class="resultat <?= $classe ?>">
                <?= $status ?>
            </section>>
            <section>
                <div class="resume"> 
                    <ul>
                    <?php 
                    if($classe ==="lose"){
			echo "<p class=\"center resultat\">" . MESSAGE_LOOSE . "</p>";
/* Affichage des bonnes réponses

                        for($i=0; $i < NB_QUESTION;$i++){ 
                           $question = getQuestionById($_SESSION['question_'.$i]);
                            if($_SESSION['reponse_'.$i] != $_SESSION['good_'.$i]){
                                echo "<li><b>" . $question['question'] . "</b> 
                                    <ul>
                                        <li class=\"fail\">Votre réponse: " . $_SESSION['reponse_'.$i] . "</li>
                                        <li class=\"good\">Bonne réponse: " . $_SESSION['good_'.$i]    . "</li>
                                    </ul>
                                </li>";
                            }
                        }
*/
                    } else { 
                        echo "<p class=\"center resultat\">" . MESSAGE_WIN . "</p>";
                    }
                    ?>
                    </ul>
                </div> 
            </section>
            <section class="section menu center">
                <a href="index.php"> <button class="btn btn-new btn-gry" type="button">Retourner au debut</button>    </a>    
            </section>
        </div>  
    </body>
</html>
