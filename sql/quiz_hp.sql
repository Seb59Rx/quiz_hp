/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 50719
 Source Host           : localhost:3306
 Source Schema         : quiz_hp

 Target Server Type    : MySQL
 Target Server Version : 50719
 File Encoding         : 65001

 Date: 24/10/2019 00:41:50
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for ludi_categories
-- ----------------------------
DROP TABLE IF EXISTS `ludi_categories`;
CREATE TABLE `ludi_categories`  (
  `id` int(11) NOT NULL,
  `category_name` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ludi_categories
-- ----------------------------
INSERT INTO `ludi_categories` VALUES (1, 'Personnages');
INSERT INTO `ludi_categories` VALUES (2, 'Poudlard');
INSERT INTO `ludi_categories` VALUES (3, 'Professeurs');
INSERT INTO `ludi_categories` VALUES (4, 'Animaux');
INSERT INTO `ludi_categories` VALUES (5, 'Creatures');

-- ----------------------------
-- Table structure for ludi_questions
-- ----------------------------
DROP TABLE IF EXISTS `ludi_questions`;
CREATE TABLE `ludi_questions`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `id_category` int(255) NULL DEFAULT NULL,
  `id_reponse` int(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 156 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ludi_questions
-- ----------------------------
INSERT INTO `ludi_questions` VALUES (1, 'Qui Sirius Black est-il venu tuer à Poudlard ?', 1, 5);
INSERT INTO `ludi_questions` VALUES (2, 'Qui donne la carte du maraudeur a Harry ?', 1, 17);
INSERT INTO `ludi_questions` VALUES (3, 'Qui apprend a Harry qu\'il est le filleul de Sirius Black ?', 1, 12);
INSERT INTO `ludi_questions` VALUES (4, 'Lequels de ces anciens élèves de Poudlard n\'est pas un Animagus ?', 1, 7);
INSERT INTO `ludi_questions` VALUES (5, 'Qui effraie la grosse dame ?', 1, 8);
INSERT INTO `ludi_questions` VALUES (6, 'Qui remplace le porfesseur Lupin quand il ne peut pas donner son cours ?', 1, 7);
INSERT INTO `ludi_questions` VALUES (7, 'Qui est la soeur de l\'oncle Vernon ?', 1, 16);
INSERT INTO `ludi_questions` VALUES (8, 'Qui a un chien pourri gaté prénommé Molaire', 1, 16);
INSERT INTO `ludi_questions` VALUES (9, 'Alors qu\'il essaie de localiser Peter Pettigrow sur la carte du maraudeur, Hary est surpris par ... ', 1, 7);
INSERT INTO `ludi_questions` VALUES (10, 'Qui est trainé dans un trou entre les racines du saule cogneur par un gros chien noir ?', 1, 3);
INSERT INTO `ludi_questions` VALUES (29, 'Qui vomit des limaces quand le sort qu\'il lance se retourne contre lui ?', 1, 3);
INSERT INTO `ludi_questions` VALUES (11, 'Qui aurait aimé capturer Sirius Black', 1, 7);
INSERT INTO `ludi_questions` VALUES (12, 'Le chien noir qui traine Ron dans un grand trou entre les racines du saule cogneur est en réalité ... ', 1, 8);
INSERT INTO `ludi_questions` VALUES (13, 'Qui neutralise le professeur Rogue dans la cabane hurlante ?', 1, 1);
INSERT INTO `ludi_questions` VALUES (14, 'Qui confisque la carte du maraudeur a Harry ?', 1, 6);
INSERT INTO `ludi_questions` VALUES (15, 'Qui Harry trouve-t-il en train de mourir au bord du lac', 1, 8);
INSERT INTO `ludi_questions` VALUES (16, 'De qui le ministère de la magie accuse Sirius Black du meurtre ? ', 1, 5);
INSERT INTO `ludi_questions` VALUES (17, 'Alors qu\'il est enfermé dans sa chambre, qui Harry voit-il par la fenêtre dans une voiture volante ?', 1, 18);
INSERT INTO `ludi_questions` VALUES (18, 'A qui demande-t-on de sauver Ginny dans la chambre des secrets', 1, 19);
INSERT INTO `ludi_questions` VALUES (19, 'Quand Harry voyage seul dans le passé, il voit le jeune Jedusor parler avec ...', 1, 11);
INSERT INTO `ludi_questions` VALUES (20, 'Qui est sequestré dans la chambre des secrets ?', 1, 21);
INSERT INTO `ludi_questions` VALUES (21, 'Qui entre contre son gré dans la chambre des secret avec Harry et Ron ?', 1, 19);
INSERT INTO `ludi_questions` VALUES (22, 'Quel élève de Gryffondor est concidéré comme etant l\'héritier de serpentar ?', 1, 1);
INSERT INTO `ludi_questions` VALUES (23, 'Selon la légende, quel fondateur de Poudlard a construit la chambre des secrets ?', 2, 24);
INSERT INTO `ludi_questions` VALUES (24, 'Qui prétend qu\'il aurait pu sauver Miss Teigne si il avait été présent quand elle a été pétrifiée ?', 1, 19);
INSERT INTO `ludi_questions` VALUES (25, 'Qui est retrouvé pétrifié, son appareil photo à la main', 1, 27);
INSERT INTO `ludi_questions` VALUES (26, 'Qui est résponsable de l\'infirmerie de Poudlard', 3, 28);
INSERT INTO `ludi_questions` VALUES (27, 'Qui tente de réparer le bras de Harry quand celui-ci se blesse au Quidditch ?', 1, 19);
INSERT INTO `ludi_questions` VALUES (28, 'Quel professeur a dans sa classe un tableau le representant ?', 3, 34);
INSERT INTO `ludi_questions` VALUES (30, 'Le Basilic de la chambre des secret est ...', 4, 38);
INSERT INTO `ludi_questions` VALUES (31, 'Qui dit à Harry et Ron que Hagrid n\'a pas ouvert la chambre des secrets ?', 5, 41);
INSERT INTO `ludi_questions` VALUES (32, 'Quel nom est gravé sur l\'épée que Harry uilise pour tuer le basilic ?', 2, 22);
INSERT INTO `ludi_questions` VALUES (33, 'Dans le film, qui répare les lunettes d\'Harry en utilisant le sort \"Oculus Reparo\"', 1, 2);
INSERT INTO `ludi_questions` VALUES (34, 'Qui est le capitaine de l\'equipe de Quidditch de Gryffondor ?', 6, 53);
INSERT INTO `ludi_questions` VALUES (35, 'Quel est le nom du rat de Ron ?', 5, 47);
INSERT INTO `ludi_questions` VALUES (36, 'Quelle couleur prend un Rapeltout quand il doit signifier a quelqu\'un qu\'il a oublié quelque chose ?', 7, 55);
INSERT INTO `ludi_questions` VALUES (37, 'Qui est condamné a mort par le département de contrôle et de régulation des créatures magiques ?', 5, 43);
INSERT INTO `ludi_questions` VALUES (38, 'Quel animal se transforme en Peter Pettigrow ?', 5, 47);
INSERT INTO `ludi_questions` VALUES (39, 'Depuis combien de temps Peter Pettigrow se cache-t-il sous l\'apparence de Croûtard ?', 8, 62);
INSERT INTO `ludi_questions` VALUES (40, 'Quel animal disparait pendant une partie de l\'année, au grand dam de Ron ?', 5, 47);
INSERT INTO `ludi_questions` VALUES (41, 'Quel bâtiment se trouve au bout du tunnel dont l\'entrée est sous le saule cogneur ?', 9, 65);
INSERT INTO `ludi_questions` VALUES (42, 'Selon Ron, quel est l\'endroit de Pré-au--Lard le plus hanté de Grande-Bretagne ?', 9, 65);
INSERT INTO `ludi_questions` VALUES (43, 'Quelle formule magique permet de neutraliser un Epouvantard ?', 10, 71);
INSERT INTO `ludi_questions` VALUES (44, 'Quelle formule Harry utilise-t-il dans son lit a Privet Drive ?', 10, 77);
INSERT INTO `ludi_questions` VALUES (45, 'Quel est le prénom de la soeur de l\'oncle Vernon ?', 11, 83);
INSERT INTO `ludi_questions` VALUES (46, 'Pour défendre Harry, Ron et Hermione contre le professeur Lupin transformé en loup-garou, Sirius Black se transforme en ...', 4, 85);
INSERT INTO `ludi_questions` VALUES (47, 'Quelle formule Harry utilise-t-il pour désarmer le professeur Rogue dans la cabane hurlante ?', 10, 70);
INSERT INTO `ludi_questions` VALUES (48, 'Quelle créature attaque Harry, Ron et Hermione quand ils quittent la Cabane hurlante ?', 4, 89);
INSERT INTO `ludi_questions` VALUES (49, 'Le professeur Lupin reconnait devant Harry, Ron et Hermione qu\'il est ...', 4, 89);
INSERT INTO `ludi_questions` VALUES (50, 'Quelle formule permet de repousser les Détraqueurs ?', 10, 80);
INSERT INTO `ludi_questions` VALUES (51, 'Qui tente d\'empêcher Harry et Hermione de suivre Ron dans un tunnel ?', 4, 93);
INSERT INTO `ludi_questions` VALUES (52, 'Après son meurtre supposé; quelle partie du corps de Peter Pettigrow est retrouvés ?', 12, 94);
INSERT INTO `ludi_questions` VALUES (53, 'Pour sauver sirius, combien de tour le professeur Dumbledore conseille-t-il à Hermione de donner au retourneur de temps ?', 13, 101);
INSERT INTO `ludi_questions` VALUES (54, 'La nuit où il quitte la maison des Dursley, Harry voit dans le parc ... ', 4, 85);
INSERT INTO `ludi_questions` VALUES (55, 'Grâce a sa cape d\'invisibilité, où Harry parvient-il a entrer bien  que l\'endroit soit interdit aux mineurs ?', 9, 66);
INSERT INTO `ludi_questions` VALUES (56, 'De quelle créature Harry croit-il voir la silhouette durant un match de Quidditch contre Poufsouffle ?', 4, 85);
INSERT INTO `ludi_questions` VALUES (57, 'Quel est le nom de l\'hippogriffe que Hagrid présente à ses élèves ?', 5, 43);
INSERT INTO `ludi_questions` VALUES (58, 'Quel est l\'endroit où le professeur Dumbledore fait dormir tous les éléves la nuit où il apprend que sirius Black est à Poudlard   ?', 14, 106);
INSERT INTO `ludi_questions` VALUES (59, 'Quel annimal apparaït lorsque Harry, Ron et Hermione sont à la recherche de Croûtard ?', 4, 85);
INSERT INTO `ludi_questions` VALUES (60, 'Que manque-t-il à l\'une des pattes de Croûtard ?', 12, 94);
INSERT INTO `ludi_questions` VALUES (61, 'A Poudlard, quel cours nécessite des tasses a thé et des boules de cristal ?', 15, 120);
INSERT INTO `ludi_questions` VALUES (62, 'Pour ouvrir une porte verrouillée, Hermione utilise la formule ...', 10, 73);
INSERT INTO `ludi_questions` VALUES (63, 'Quel est le nom de la gare de Poudlard', 16, 133);
INSERT INTO `ludi_questions` VALUES (64, 'Quel est le nom de la chouette de Harry ?', 5, 46);
INSERT INTO `ludi_questions` VALUES (65, 'Qui est résponsable de la cicatrice sur le front d\'Harry ?', 1, 13);
INSERT INTO `ludi_questions` VALUES (66, 'Selon le Choixpeau, dans quelle maison Harry pourrait-il aller ? ', 17, 139);
INSERT INTO `ludi_questions` VALUES (67, 'Quel professeur est le directeur de Serpentard ?', 3, 31);
INSERT INTO `ludi_questions` VALUES (68, 'Selon le professeur McGonagall, c\'est un privilège de visiter ...', 16, 133);
INSERT INTO `ludi_questions` VALUES (69, 'Quelle forme prend l\'épouvantard de Harry ?', 4, 154);
INSERT INTO `ludi_questions` VALUES (70, 'Pendant le match de Quidditch sous l\'orage, Harry tombe de son balai suita à l\'attaque d\'un ... ', 18, 142);
INSERT INTO `ludi_questions` VALUES (71, 'Quelle forme prend l\'épouvantard de Ron ?', 4, 37);
INSERT INTO `ludi_questions` VALUES (72, 'Quel animal emmène Harry voler au dessus de Poudlard ?', 5, 43);
INSERT INTO `ludi_questions` VALUES (73, 'Alors qu\'il essaie de localiser Peter Pettigrow sur la carte du maraudeur, Harry est surpris par ... ', 3, 7);
INSERT INTO `ludi_questions` VALUES (74, 'Qui est trainé dans un trou entre les racines du saule cogneur par un gros chien noir ?', 1, 3);
INSERT INTO `ludi_questions` VALUES (75, 'Quelle est la forme du patronus que Harry apperçois au bord du lac ?', 4, 155);
INSERT INTO `ludi_questions` VALUES (76, 'Quel cadeau Harry reçoit-il à Noël ?', 19, 156);
INSERT INTO `ludi_questions` VALUES (77, 'Combien y a-t-il de sortes de balles de Quidditch ?', 13, 101);
INSERT INTO `ludi_questions` VALUES (78, 'Quel est le nom du crapeau de Neville', 5, 160);
INSERT INTO `ludi_questions` VALUES (79, 'Dans le paquet que Harry reçoit, la première année, par hibou dans la Grande Salle se trouve ...', 19, 161);
INSERT INTO `ludi_questions` VALUES (80, 'Qui emmène Harry au Chaudraon Baveur pour la première fois ?', 1, 15);
INSERT INTO `ludi_questions` VALUES (81, 'Quel cadeau de Noël Ron reçoit-il de la part de sa mère ?', 19, 164);
INSERT INTO `ludi_questions` VALUES (82, 'Qui dirige la partie d\'echecs géants à  laquelle jouent Harry, Ron et Hermione', 1, 3);
INSERT INTO `ludi_questions` VALUES (83, 'Quelle créature attaque Hermione dans les toilettes des filles ? ', 4, 88);
INSERT INTO `ludi_questions` VALUES (84, 'Dans la forêt interdite, Harry est sauvé de Voldemort pas ... ', 4, 86);
INSERT INTO `ludi_questions` VALUES (85, 'Dans leur quête de la pièrre philisophale, Harry, Ron et Hermione doivent attraper un objet. De quoi s\'agit-il ?', 19, 166);
INSERT INTO `ludi_questions` VALUES (86, 'Combien de joueurs compose une équipe de Quidditch ?', 13, 168);
INSERT INTO `ludi_questions` VALUES (87, 'Quelle créature sort de l\'oeuf que Hagrid a gagné à une partie de cartes dans un pub ?', 4, 173);
INSERT INTO `ludi_questions` VALUES (88, 'Quel instrument de musique Harry, Ron et Hermione touvent-ils à coté de Touffu ?', 20, 174);
INSERT INTO `ludi_questions` VALUES (89, 'Qui utilise la formule \"Petrificus Totalus\" pour immobiliser Neville et quitter la tour de Gryffondor ?', 1, 2);
INSERT INTO `ludi_questions` VALUES (90, 'Qui est le professeur de potions à Poudlard, en première année ?', 3, 31);
INSERT INTO `ludi_questions` VALUES (91, 'Lequel des animaux de Hagrid est envoyé en Roumanie ?', 5, 58);
INSERT INTO `ludi_questions` VALUES (92, 'Quels sont les couleurs de Gryffondor ?', 21, 179);
INSERT INTO `ludi_questions` VALUES (93, 'Quels sont les couleurs de Serpentard ?', 21, 180);
INSERT INTO `ludi_questions` VALUES (94, 'Quels sont les couleurs de Poufsouffle ?', 21, 181);
INSERT INTO `ludi_questions` VALUES (95, 'Quels sont les couleurs de Serredaigle ?', 21, 182);
INSERT INTO `ludi_questions` VALUES (96, 'Qui se voit en capitaine de l\'équipe de Quidditch dans le Mirroir de Riséd ?', 1, 3);
INSERT INTO `ludi_questions` VALUES (97, 'Qui dénonce Hagrid parce que celui-ci élève un bébé dragon dans sa cabane ? ', 1, 4);
INSERT INTO `ludi_questions` VALUES (98, 'Dans le Poudlard Express, qu\'essaie de faire Ron à son rat, sans succès ?', 22, 184);
INSERT INTO `ludi_questions` VALUES (99, 'Pourquoi Harry peut-il entendre le Basilic ? ', 23, 187);
INSERT INTO `ludi_questions` VALUES (100, 'Grâce au journal de tom Jedusor, de combien d\'années Harry retourne-t-il dans le passé ?', 8, 64);
INSERT INTO `ludi_questions` VALUES (101, 'Selon le professeur Dumbledor, le Miroir du Risèd montre a celui qui se place devant lui ...', 24, 191);
INSERT INTO `ludi_questions` VALUES (102, 'Le professeur Dumbledor ordonne aux fantomes de cherchez la grosse dame dans ...', 9, 195);
INSERT INTO `ludi_questions` VALUES (103, 'Chez les Weasley, quel objet indique l\'endroit où se trouve chaque membre de la famille ?', 19, 198);
INSERT INTO `ludi_questions` VALUES (104, 'Quel est le titre de l\'autobiographie de Gilderoy Lockhart ?', 25, 200);
INSERT INTO `ludi_questions` VALUES (105, 'Qu\'est-ce qu\'un fourchelang ? ', 26, 206);
INSERT INTO `ludi_questions` VALUES (106, 'Quelle créature sauve Harry grâce à ses larmes quand celui-ci est bléssé par le Basilic ?', 5, 44);
INSERT INTO `ludi_questions` VALUES (107, 'Selon la légende, dans la chambre des secrets se trouve ...', 19, 207);
INSERT INTO `ludi_questions` VALUES (108, 'Quelle potion Madame Pomfresh administre-t-elle à Harry pour lui faire repousser les os de son bras ?', 27, 211);
INSERT INTO `ludi_questions` VALUES (109, 'Chez Ollivander, combien de baguettes Harry essaie-t-il avant de trouver la sienne ?', 13, 100);
INSERT INTO `ludi_questions` VALUES (110, 'Lors de sa première visite au Chaudron Baveur, Harry rencontre ...', 3, 33);
INSERT INTO `ludi_questions` VALUES (111, 'Quel est le prénom de la mère de Harry ?', 28, 214);
INSERT INTO `ludi_questions` VALUES (112, 'Qui retrouve Harry dans la cabane au sommet du rocher ?', 1, 15);
INSERT INTO `ludi_questions` VALUES (113, 'La lettre que Harry reçoit de Poudlard mentionne qu\'il peut apporter une chouette, un chat ou ...', 4, 219);
INSERT INTO `ludi_questions` VALUES (114, 'Qui explique à Harry comment on accède au quai 9 3/4 ?', 1, 222);
INSERT INTO `ludi_questions` VALUES (115, 'Quand est née la mère de Harry ?', 29, 223);
INSERT INTO `ludi_questions` VALUES (116, 'Quand est morte la mère de Harry', 29, 224);
INSERT INTO `ludi_questions` VALUES (117, 'Quand est né le père de Harry ?', 29, 225);
INSERT INTO `ludi_questions` VALUES (118, 'Quand est mort le père de Harry ?', 29, 224);
INSERT INTO `ludi_questions` VALUES (119, 'Quand est né Voldemort ?', 29, 229);
INSERT INTO `ludi_questions` VALUES (120, 'Quand est mort Voldemort ?', 29, 228);
INSERT INTO `ludi_questions` VALUES (121, 'De quelle maison fait partie Luna Lovegood ?', 17, 141);
INSERT INTO `ludi_questions` VALUES (122, 'De qui Bellatrix Lestrange est-elle la tante ?', 1, 4);
INSERT INTO `ludi_questions` VALUES (123, 'Qand est né Harry Potter ?', 29, 230);
INSERT INTO `ludi_questions` VALUES (124, 'Qui a écrit \"Vie et habitat des animaux fantastiques\" ?', 30, 231);
INSERT INTO `ludi_questions` VALUES (125, 'Quelle est la phrase inscrite sur le miroir du Risèd ?', 31, 237);
INSERT INTO `ludi_questions` VALUES (126, 'A quelle page se situe le cours sur les loups-garous donné par Rogue ?', 32, 243);
INSERT INTO `ludi_questions` VALUES (127, 'Qui raffole du sirop de cerises et soda avec boule de glace et ombrelle ?', 3, 244);
INSERT INTO `ludi_questions` VALUES (128, 'A qui appartenait le cheveu de Vélane contenu dans la baguette de Fleur Delacour ?', 33, 246);
INSERT INTO `ludi_questions` VALUES (129, 'Que contient la baguette de Cédric Diggory ?', 34, 256);
INSERT INTO `ludi_questions` VALUES (130, 'Que contient la baguette de Tom Jedusor ?', 34, 257);
INSERT INTO `ludi_questions` VALUES (131, 'Que contient la baguette de Dolores Ombrage ?', 34, 255);
INSERT INTO `ludi_questions` VALUES (132, 'Que contient la baguette de Albus Dumbledore ?', 34, 260);
INSERT INTO `ludi_questions` VALUES (133, 'Quel est le numéro du décret qui interdit la réunion d\'élèves ?', 35, 264);
INSERT INTO `ludi_questions` VALUES (134, 'Où est né le boa constrictor qui \'attaque\' Dudley dans le tome 1 ?', 36, 272);
INSERT INTO `ludi_questions` VALUES (135, 'Combien de fois Gilderoy Lockhart a-t-il obtenu le prix du sourire le plus charmeur ?', 13, 103);
INSERT INTO `ludi_questions` VALUES (136, 'Quel professeur explique l\'histoire de la chambre des secrets dans le livre ?', 3, 275);
INSERT INTO `ludi_questions` VALUES (137, 'Quelle insulte Malefoy lance-t-il à Buck ?', 37, 276);
INSERT INTO `ludi_questions` VALUES (138, 'Quel élève a eu le plus de mal à devenir animagus ?', 1, 5);
INSERT INTO `ludi_questions` VALUES (139, 'A quel procès Harry assiste-t-il en premier dans la pensine ?', 38, 281);
INSERT INTO `ludi_questions` VALUES (140, 'Selon le chicaneur, quel serait le vrai nom de Sirius Black ?', 39, 288);
INSERT INTO `ludi_questions` VALUES (141, 'Comment s\'appelle la capitaine des Harpies de Holyhead ?', 40, 291);
INSERT INTO `ludi_questions` VALUES (142, 'Qui meurt en premier dans le dernier tome ?', 40, 296);
INSERT INTO `ludi_questions` VALUES (143, 'Qui était l\'ancêtre d\'Harry ?', 39, 299);
INSERT INTO `ludi_questions` VALUES (144, 'Où se situe le diadème perdu ?', 14, 116);
INSERT INTO `ludi_questions` VALUES (145, 'Qui a tué Nagini ?', 1, 36);
INSERT INTO `ludi_questions` VALUES (146, 'Quelle était la devise du Ministère de la magie quand il était contrôlé par Voldemort ?', 41, 301);
INSERT INTO `ludi_questions` VALUES (147, 'Quel objet est la relique d\'Helga Poufsouffle ? ', 42, 305);
INSERT INTO `ludi_questions` VALUES (148, 'Quelle est la particularité d\'Argus Rusard ? ', 43, 311);
INSERT INTO `ludi_questions` VALUES (149, 'Quel est le nom de l\'organisation fondée par Hermione pour améliorer les conditions de vie des elfes de maison ? ', 44, 317);
INSERT INTO `ludi_questions` VALUES (150, 'Comment se nomme la journaliste sorcière à sensation ? ', 45, 321);
INSERT INTO `ludi_questions` VALUES (151, 'Quelle est la seule personne dont Peeves a peur ? ', 1, 325);
INSERT INTO `ludi_questions` VALUES (152, 'Quel est le patronus de Ginny Weasley ? ', 4, 326);
INSERT INTO `ludi_questions` VALUES (153, 'Dans les livres, qui indique à Harry l\'existence de la Salle sur Demande ? ', 5, 45);
INSERT INTO `ludi_questions` VALUES (154, 'Quel dragon Cédric Diggory doit-il affronter lors de la première tâche du Tournoi des Trois Sorciers ? ', 46, 330);
INSERT INTO `ludi_questions` VALUES (155, 'Que faut-il chatouiller sur un tableau pour accéder aux cuisines de Poudlard ? ', 47, 331);

-- ----------------------------
-- Table structure for ludi_reponses
-- ----------------------------
DROP TABLE IF EXISTS `ludi_reponses`;
CREATE TABLE `ludi_reponses`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reponse` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `id_category` int(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 337 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ludi_reponses
-- ----------------------------
INSERT INTO `ludi_reponses` VALUES (1, 'Harry Potter', 1);
INSERT INTO `ludi_reponses` VALUES (2, 'Hermione Grandger', 1);
INSERT INTO `ludi_reponses` VALUES (3, 'Ron Weasley', 1);
INSERT INTO `ludi_reponses` VALUES (4, 'Drago Malfoy', 1);
INSERT INTO `ludi_reponses` VALUES (5, 'Peter Pettigrow', 1);
INSERT INTO `ludi_reponses` VALUES (6, 'Le professeur Lupin', 1);
INSERT INTO `ludi_reponses` VALUES (7, 'Le professeur Rogue', 1);
INSERT INTO `ludi_reponses` VALUES (8, 'Sirius Black', 1);
INSERT INTO `ludi_reponses` VALUES (9, 'Barty Croupton junior', 1);
INSERT INTO `ludi_reponses` VALUES (10, 'Le professeur Quirrell', 1);
INSERT INTO `ludi_reponses` VALUES (11, 'Le professeur Dumbledore', 1);
INSERT INTO `ludi_reponses` VALUES (12, 'Le professeur McGonagall', 1);
INSERT INTO `ludi_reponses` VALUES (13, 'Lord Voldemort', 1);
INSERT INTO `ludi_reponses` VALUES (14, 'Dean Thomas', 1);
INSERT INTO `ludi_reponses` VALUES (15, 'Hagrid', 1);
INSERT INTO `ludi_reponses` VALUES (16, 'Tante Marge', 1);
INSERT INTO `ludi_reponses` VALUES (17, 'Les jumeaux Weasley', 1);
INSERT INTO `ludi_reponses` VALUES (18, 'Fred, George et Ron Weasley', 1);
INSERT INTO `ludi_reponses` VALUES (19, 'Le professeur Lockart', 1);
INSERT INTO `ludi_reponses` VALUES (20, 'Le professeur Drippet', 1);
INSERT INTO `ludi_reponses` VALUES (21, 'Ginny Weasley', 1);
INSERT INTO `ludi_reponses` VALUES (22, 'Godric Gryffondor', 2);
INSERT INTO `ludi_reponses` VALUES (23, 'Helga Poufsouffle', 2);
INSERT INTO `ludi_reponses` VALUES (24, 'Salazar Serpentard', 2);
INSERT INTO `ludi_reponses` VALUES (25, 'Rowena Serdaigle', 2);
INSERT INTO `ludi_reponses` VALUES (26, 'Olivier Dubois', 1);
INSERT INTO `ludi_reponses` VALUES (27, 'Colin Crivey', 1);
INSERT INTO `ludi_reponses` VALUES (28, 'Madame Pomfresh', 3);
INSERT INTO `ludi_reponses` VALUES (29, 'Le professeur Dumbledore', 3);
INSERT INTO `ludi_reponses` VALUES (30, 'Le professeur McGonagall', 3);
INSERT INTO `ludi_reponses` VALUES (31, 'Le professeur Rogue', 3);
INSERT INTO `ludi_reponses` VALUES (32, 'Le professeur Lupin', 3);
INSERT INTO `ludi_reponses` VALUES (33, 'Le professeur Quirrell', 3);
INSERT INTO `ludi_reponses` VALUES (34, 'Le professeur Lockart', 3);
INSERT INTO `ludi_reponses` VALUES (35, 'Le professeur Drippet', 3);
INSERT INTO `ludi_reponses` VALUES (36, 'Neville Londubat', 1);
INSERT INTO `ludi_reponses` VALUES (37, 'Une araignée', 4);
INSERT INTO `ludi_reponses` VALUES (38, 'Un serpent', 4);
INSERT INTO `ludi_reponses` VALUES (39, 'Un aigle', 4);
INSERT INTO `ludi_reponses` VALUES (40, 'Un loup', 4);
INSERT INTO `ludi_reponses` VALUES (41, 'Aragog', 5);
INSERT INTO `ludi_reponses` VALUES (42, 'Firenze', 5);
INSERT INTO `ludi_reponses` VALUES (43, 'Buck', 5);
INSERT INTO `ludi_reponses` VALUES (44, 'Fumeseck', 5);
INSERT INTO `ludi_reponses` VALUES (45, 'Dobby', 5);
INSERT INTO `ludi_reponses` VALUES (46, 'Hedwige', 5);
INSERT INTO `ludi_reponses` VALUES (47, 'Croûtard', 5);
INSERT INTO `ludi_reponses` VALUES (48, 'Pattenrond', 5);
INSERT INTO `ludi_reponses` VALUES (49, 'Harry Potter', 6);
INSERT INTO `ludi_reponses` VALUES (50, 'Olivier Diggory', 6);
INSERT INTO `ludi_reponses` VALUES (51, 'Cedric Diggory', 6);
INSERT INTO `ludi_reponses` VALUES (52, 'Seamus Finigan', 6);
INSERT INTO `ludi_reponses` VALUES (53, 'Olivier Dubois', 6);
INSERT INTO `ludi_reponses` VALUES (54, 'Vert', 7);
INSERT INTO `ludi_reponses` VALUES (55, 'Rouge', 7);
INSERT INTO `ludi_reponses` VALUES (56, 'Bleu', 7);
INSERT INTO `ludi_reponses` VALUES (57, 'Jaune', 7);
INSERT INTO `ludi_reponses` VALUES (58, 'Norbert', 5);
INSERT INTO `ludi_reponses` VALUES (59, 'Le gros chien noir', 5);
INSERT INTO `ludi_reponses` VALUES (60, 'Un an', 8);
INSERT INTO `ludi_reponses` VALUES (61, 'Cinq ans', 8);
INSERT INTO `ludi_reponses` VALUES (62, 'Douze ans', 8);
INSERT INTO `ludi_reponses` VALUES (63, 'Vingt sept ans', 8);
INSERT INTO `ludi_reponses` VALUES (64, 'Cinquante ans', 8);
INSERT INTO `ludi_reponses` VALUES (65, 'La Cabane hurlante', 9);
INSERT INTO `ludi_reponses` VALUES (66, 'Les Trois Balais', 9);
INSERT INTO `ludi_reponses` VALUES (67, 'Honneydukes', 9);
INSERT INTO `ludi_reponses` VALUES (68, 'Le Chaudron Baveur', 9);
INSERT INTO `ludi_reponses` VALUES (69, 'Le square Grimmaurd', 9);
INSERT INTO `ludi_reponses` VALUES (70, 'Expelliarmus', 10);
INSERT INTO `ludi_reponses` VALUES (71, 'Riddikulus', 10);
INSERT INTO `ludi_reponses` VALUES (72, 'Impedimenta', 10);
INSERT INTO `ludi_reponses` VALUES (73, 'Alohomora', 10);
INSERT INTO `ludi_reponses` VALUES (74, 'Wingardium leviosa', 10);
INSERT INTO `ludi_reponses` VALUES (75, 'Occulus reparo', 10);
INSERT INTO `ludi_reponses` VALUES (76, 'Assendiato', 10);
INSERT INTO `ludi_reponses` VALUES (77, 'Lumos maxima', 10);
INSERT INTO `ludi_reponses` VALUES (78, 'Bombarda', 10);
INSERT INTO `ludi_reponses` VALUES (79, 'Accio', 10);
INSERT INTO `ludi_reponses` VALUES (80, 'Expecto Patronum', 10);
INSERT INTO `ludi_reponses` VALUES (81, 'Bathilda', 11);
INSERT INTO `ludi_reponses` VALUES (82, 'Hedwige', 11);
INSERT INTO `ludi_reponses` VALUES (83, 'Marge', 11);
INSERT INTO `ludi_reponses` VALUES (84, 'Molly', 11);
INSERT INTO `ludi_reponses` VALUES (85, 'Un chien', 4);
INSERT INTO `ludi_reponses` VALUES (86, 'Un centaure', 4);
INSERT INTO `ludi_reponses` VALUES (87, 'Un taureau', 4);
INSERT INTO `ludi_reponses` VALUES (88, 'Un troll', 4);
INSERT INTO `ludi_reponses` VALUES (89, 'Un loup-garou', 4);
INSERT INTO `ludi_reponses` VALUES (90, 'Un hippogriffe', 4);
INSERT INTO `ludi_reponses` VALUES (91, 'Un animagus', 4);
INSERT INTO `ludi_reponses` VALUES (92, 'Un mangemort', 4);
INSERT INTO `ludi_reponses` VALUES (93, 'Le Saule cogneur', 4);
INSERT INTO `ludi_reponses` VALUES (94, 'Un doigt', 12);
INSERT INTO `ludi_reponses` VALUES (95, 'Une oreille', 12);
INSERT INTO `ludi_reponses` VALUES (96, 'Un pied', 12);
INSERT INTO `ludi_reponses` VALUES (97, 'Une main', 12);
INSERT INTO `ludi_reponses` VALUES (98, 'Un orteil', 12);
INSERT INTO `ludi_reponses` VALUES (99, 'Un', 13);
INSERT INTO `ludi_reponses` VALUES (100, 'Deux', 13);
INSERT INTO `ludi_reponses` VALUES (101, 'Trois', 13);
INSERT INTO `ludi_reponses` VALUES (102, 'Quatre', 13);
INSERT INTO `ludi_reponses` VALUES (103, 'Cinq', 13);
INSERT INTO `ludi_reponses` VALUES (104, 'Crockdur', 5);
INSERT INTO `ludi_reponses` VALUES (105, 'La bibliothèque', 14);
INSERT INTO `ludi_reponses` VALUES (106, 'La Grande Salle', 14);
INSERT INTO `ludi_reponses` VALUES (107, 'Le Hall d\'entrée', 14);
INSERT INTO `ludi_reponses` VALUES (108, 'La salle commune de Gryffondor', 14);
INSERT INTO `ludi_reponses` VALUES (109, 'La salle commune de Poufsoufle', 14);
INSERT INTO `ludi_reponses` VALUES (110, 'La salle commune de Serpentard', 14);
INSERT INTO `ludi_reponses` VALUES (111, 'La salle commune de Serdaigle', 14);
INSERT INTO `ludi_reponses` VALUES (112, 'Le bureau du professuer Bumbledor', 14);
INSERT INTO `ludi_reponses` VALUES (113, 'Les cachots', 14);
INSERT INTO `ludi_reponses` VALUES (114, 'L\'infirmerie', 14);
INSERT INTO `ludi_reponses` VALUES (115, 'La salle des trophées', 14);
INSERT INTO `ludi_reponses` VALUES (116, 'La salle sur demande', 14);
INSERT INTO `ludi_reponses` VALUES (117, 'La chambre des secrets', 14);
INSERT INTO `ludi_reponses` VALUES (118, 'La volière', 14);
INSERT INTO `ludi_reponses` VALUES (119, 'Un ongle', 12);
INSERT INTO `ludi_reponses` VALUES (120, 'La divination', 15);
INSERT INTO `ludi_reponses` VALUES (121, 'Les potions', 15);
INSERT INTO `ludi_reponses` VALUES (122, 'La botanique', 15);
INSERT INTO `ludi_reponses` VALUES (123, 'L\'astronomie', 15);
INSERT INTO `ludi_reponses` VALUES (124, 'La défense contre les forces du mal', 15);
INSERT INTO `ludi_reponses` VALUES (125, 'L\'histoire de la magie', 15);
INSERT INTO `ludi_reponses` VALUES (126, 'La métamorphose', 15);
INSERT INTO `ludi_reponses` VALUES (127, 'Les sortilèges', 15);
INSERT INTO `ludi_reponses` VALUES (128, 'Le vol sur un balai', 15);
INSERT INTO `ludi_reponses` VALUES (129, 'L\'arithmancie', 15);
INSERT INTO `ludi_reponses` VALUES (130, 'L\'étude des moldus', 15);
INSERT INTO `ludi_reponses` VALUES (131, 'L\'étude des runes', 15);
INSERT INTO `ludi_reponses` VALUES (132, 'Petrificus Totalus', 10);
INSERT INTO `ludi_reponses` VALUES (133, 'Pré-au-lard', 16);
INSERT INTO `ludi_reponses` VALUES (134, 'Little Hangleton', 16);
INSERT INTO `ludi_reponses` VALUES (135, 'Godric\'s Hollow', 16);
INSERT INTO `ludi_reponses` VALUES (136, 'Privet Drive', 16);
INSERT INTO `ludi_reponses` VALUES (137, 'King\'s Cross', 16);
INSERT INTO `ludi_reponses` VALUES (138, 'Gryffondor', 17);
INSERT INTO `ludi_reponses` VALUES (139, 'Serpentard', 17);
INSERT INTO `ludi_reponses` VALUES (140, 'Poufsouffle', 17);
INSERT INTO `ludi_reponses` VALUES (141, 'Serdaigle', 17);
INSERT INTO `ludi_reponses` VALUES (142, 'Détraqueur', 18);
INSERT INTO `ludi_reponses` VALUES (143, 'Loup-garou', 18);
INSERT INTO `ludi_reponses` VALUES (144, 'Sinistros', 18);
INSERT INTO `ludi_reponses` VALUES (145, 'Lutin de Cornouailles', 18);
INSERT INTO `ludi_reponses` VALUES (146, 'Acromantule', 18);
INSERT INTO `ludi_reponses` VALUES (147, 'Phénix ', 18);
INSERT INTO `ludi_reponses` VALUES (148, 'Hippogriffe', 18);
INSERT INTO `ludi_reponses` VALUES (149, 'Elfe de maison', 18);
INSERT INTO `ludi_reponses` VALUES (150, 'Basilic', 18);
INSERT INTO `ludi_reponses` VALUES (151, 'Sombral ', 18);
INSERT INTO `ludi_reponses` VALUES (152, 'Épouvantard ', 18);
INSERT INTO `ludi_reponses` VALUES (153, 'Mangemort', 18);
INSERT INTO `ludi_reponses` VALUES (154, 'Un détraqueur', 4);
INSERT INTO `ludi_reponses` VALUES (155, 'Un cerf', 4);
INSERT INTO `ludi_reponses` VALUES (156, 'Un eclair de feu', 19);
INSERT INTO `ludi_reponses` VALUES (157, 'La carte du Maraudeur', 19);
INSERT INTO `ludi_reponses` VALUES (158, 'La cape d\'invisibilité', 19);
INSERT INTO `ludi_reponses` VALUES (159, 'Un retourneur de temps', 19);
INSERT INTO `ludi_reponses` VALUES (160, 'Trevor', 5);
INSERT INTO `ludi_reponses` VALUES (161, 'Un Nimbus 2000', 19);
INSERT INTO `ludi_reponses` VALUES (162, 'Une robe', 19);
INSERT INTO `ludi_reponses` VALUES (163, 'Une baguette', 19);
INSERT INTO `ludi_reponses` VALUES (164, 'Un pull', 19);
INSERT INTO `ludi_reponses` VALUES (165, 'Un foulard', 19);
INSERT INTO `ludi_reponses` VALUES (166, 'Une clé volante', 19);
INSERT INTO `ludi_reponses` VALUES (167, 'Six', 13);
INSERT INTO `ludi_reponses` VALUES (168, 'Sept', 13);
INSERT INTO `ludi_reponses` VALUES (169, 'Huit', 13);
INSERT INTO `ludi_reponses` VALUES (170, 'Neuf', 13);
INSERT INTO `ludi_reponses` VALUES (171, 'Dix', 13);
INSERT INTO `ludi_reponses` VALUES (172, 'Quatorze', 13);
INSERT INTO `ludi_reponses` VALUES (173, 'Un dragon', 4);
INSERT INTO `ludi_reponses` VALUES (174, 'Une harpe', 20);
INSERT INTO `ludi_reponses` VALUES (175, 'Une flûte', 20);
INSERT INTO `ludi_reponses` VALUES (176, 'Un accordéon', 20);
INSERT INTO `ludi_reponses` VALUES (177, 'Un violon', 20);
INSERT INTO `ludi_reponses` VALUES (178, 'Touffu', 5);
INSERT INTO `ludi_reponses` VALUES (179, 'Rouge et or', 21);
INSERT INTO `ludi_reponses` VALUES (180, 'Vert et Argent', 21);
INSERT INTO `ludi_reponses` VALUES (181, 'Jaune et noir', 21);
INSERT INTO `ludi_reponses` VALUES (182, 'Bleu et gris', 21);
INSERT INTO `ludi_reponses` VALUES (183, 'De le changer en lapin', 22);
INSERT INTO `ludi_reponses` VALUES (184, 'De changer sa couleur', 22);
INSERT INTO `ludi_reponses` VALUES (185, 'De le faire floter dans l\'air', 22);
INSERT INTO `ludi_reponses` VALUES (186, 'De le faire grossir', 22);
INSERT INTO `ludi_reponses` VALUES (187, 'Parce qu\'il est Fourchelang', 23);
INSERT INTO `ludi_reponses` VALUES (188, 'Parce qu\'il est l\'héritier de Serpentard', 23);
INSERT INTO `ludi_reponses` VALUES (189, 'Parce que son esprit est connecté à celui de Voldemort', 23);
INSERT INTO `ludi_reponses` VALUES (190, 'Parce que son patronus est un Basilic', 23);
INSERT INTO `ludi_reponses` VALUES (191, 'Ses désirs', 24);
INSERT INTO `ludi_reponses` VALUES (192, 'Ses peurs', 24);
INSERT INTO `ludi_reponses` VALUES (193, 'Son avenir', 24);
INSERT INTO `ludi_reponses` VALUES (194, 'Ses mensonges', 24);
INSERT INTO `ludi_reponses` VALUES (195, 'Les tableaux du château', 9);
INSERT INTO `ludi_reponses` VALUES (196, 'A Pré-au-Lard', 9);
INSERT INTO `ludi_reponses` VALUES (197, 'Une boule de cristal', 19);
INSERT INTO `ludi_reponses` VALUES (198, 'Une pendule', 19);
INSERT INTO `ludi_reponses` VALUES (199, 'Moi, Magicien', 25);
INSERT INTO `ludi_reponses` VALUES (202, 'Moi Le Grand Magicien', 25);
INSERT INTO `ludi_reponses` VALUES (200, 'Moi, Le Magicien', 25);
INSERT INTO `ludi_reponses` VALUES (201, 'Moi, Grand Magicien', 25);
INSERT INTO `ludi_reponses` VALUES (203, 'Une personne qui parle plus de trois langues étrangères', 26);
INSERT INTO `ludi_reponses` VALUES (204, 'Une personne qui peut parler aux objets', 26);
INSERT INTO `ludi_reponses` VALUES (205, 'Une personne qui peut parler aux animaux', 26);
INSERT INTO `ludi_reponses` VALUES (206, 'Une personne qui peut parler aux serpents', 26);
INSERT INTO `ludi_reponses` VALUES (207, 'Un monstre', 19);
INSERT INTO `ludi_reponses` VALUES (208, 'un Trésor', 19);
INSERT INTO `ludi_reponses` VALUES (209, 'Un livre de sort trés puissant', 19);
INSERT INTO `ludi_reponses` VALUES (210, 'Du Retour d\'os', 27);
INSERT INTO `ludi_reponses` VALUES (211, 'Du Pousse os', 27);
INSERT INTO `ludi_reponses` VALUES (212, 'Du Revive os', 27);
INSERT INTO `ludi_reponses` VALUES (213, 'Du Repousse os', 27);
INSERT INTO `ludi_reponses` VALUES (214, 'Lily', 28);
INSERT INTO `ludi_reponses` VALUES (215, 'Rose', 28);
INSERT INTO `ludi_reponses` VALUES (216, 'Lise', 28);
INSERT INTO `ludi_reponses` VALUES (217, 'Lisa', 28);
INSERT INTO `ludi_reponses` VALUES (218, 'Elisabeth', 28);
INSERT INTO `ludi_reponses` VALUES (219, 'Un crapeau', 4);
INSERT INTO `ludi_reponses` VALUES (220, 'Une chauve-souris', 4);
INSERT INTO `ludi_reponses` VALUES (221, 'Un perroquet', 4);
INSERT INTO `ludi_reponses` VALUES (222, 'Molly Weasley', 1);
INSERT INTO `ludi_reponses` VALUES (223, '30 janvier 1960', 29);
INSERT INTO `ludi_reponses` VALUES (224, '31 octobre 1981', 29);
INSERT INTO `ludi_reponses` VALUES (225, '27 mars 1960', 29);
INSERT INTO `ludi_reponses` VALUES (226, '30 octobre 1981', 29);
INSERT INTO `ludi_reponses` VALUES (227, '9 janvier 1960', 29);
INSERT INTO `ludi_reponses` VALUES (228, '2 mai 1998', 29);
INSERT INTO `ludi_reponses` VALUES (229, '31 décembre 1926', 29);
INSERT INTO `ludi_reponses` VALUES (230, '31 juillet 1980', 29);
INSERT INTO `ludi_reponses` VALUES (231, 'Norbert Dragonneau', 30);
INSERT INTO `ludi_reponses` VALUES (232, 'Hedwige Plumesck', 30);
INSERT INTO `ludi_reponses` VALUES (233, 'Emeric G. Changé', 30);
INSERT INTO `ludi_reponses` VALUES (234, 'Phyllida Augirolle', 30);
INSERT INTO `ludi_reponses` VALUES (235, 'Edwardus Lima', 30);
INSERT INTO `ludi_reponses` VALUES (236, 'Riséd elrue ocnote dsiam egasiv notsapert nomen ej', 31);
INSERT INTO `ludi_reponses` VALUES (237, 'Riséd elrue ocnot edsi amega siv notsap ert nomen ej', 31);
INSERT INTO `ludi_reponses` VALUES (238, 'Risédel rueoc not edsi amegasiv notsap ertnom enej', 31);
INSERT INTO `ludi_reponses` VALUES (239, 'Riséd elrueoc notedsi amega sivnot sapert nomenej', 31);
INSERT INTO `ludi_reponses` VALUES (240, 'Page 391', 32);
INSERT INTO `ludi_reponses` VALUES (241, 'Page 392', 32);
INSERT INTO `ludi_reponses` VALUES (242, 'Page 393', 32);
INSERT INTO `ludi_reponses` VALUES (243, 'Page 394', 32);
INSERT INTO `ludi_reponses` VALUES (244, 'Le professeur Flitwick', 3);
INSERT INTO `ludi_reponses` VALUES (245, 'Sa mère', 33);
INSERT INTO `ludi_reponses` VALUES (246, 'Sa grand mère', 33);
INSERT INTO `ludi_reponses` VALUES (247, 'Sa soeur', 33);
INSERT INTO `ludi_reponses` VALUES (248, 'Son arrière grand mère', 33);
INSERT INTO `ludi_reponses` VALUES (249, 'Sa tante', 33);
INSERT INTO `ludi_reponses` VALUES (250, 'Son père', 33);
INSERT INTO `ludi_reponses` VALUES (251, 'Son grand père', 33);
INSERT INTO `ludi_reponses` VALUES (252, 'Son frère', 33);
INSERT INTO `ludi_reponses` VALUES (253, 'Son arrière grand père', 33);
INSERT INTO `ludi_reponses` VALUES (254, 'Son oncle', 33);
INSERT INTO `ludi_reponses` VALUES (255, 'Un ventricule de dragon', 34);
INSERT INTO `ludi_reponses` VALUES (256, 'Un crin de licorne', 34);
INSERT INTO `ludi_reponses` VALUES (257, 'Une plume de phénix', 34);
INSERT INTO `ludi_reponses` VALUES (258, 'Un nerf de coeur de dragon', 34);
INSERT INTO `ludi_reponses` VALUES (259, 'Un cheveu de Vélane', 34);
INSERT INTO `ludi_reponses` VALUES (260, 'Un crin de sombral', 34);
INSERT INTO `ludi_reponses` VALUES (261, 'Décret  N°21', 35);
INSERT INTO `ludi_reponses` VALUES (262, 'Décret  N°22', 35);
INSERT INTO `ludi_reponses` VALUES (263, 'Décret  N°23', 35);
INSERT INTO `ludi_reponses` VALUES (264, 'Décret  N°24', 35);
INSERT INTO `ludi_reponses` VALUES (265, 'Décret  N°25', 35);
INSERT INTO `ludi_reponses` VALUES (266, 'Décret  N°26', 35);
INSERT INTO `ludi_reponses` VALUES (267, 'Décret  N°27', 35);
INSERT INTO `ludi_reponses` VALUES (268, 'Décret  N°28', 35);
INSERT INTO `ludi_reponses` VALUES (269, 'Décret  N°29', 35);
INSERT INTO `ludi_reponses` VALUES (270, 'Brésil', 36);
INSERT INTO `ludi_reponses` VALUES (271, 'Argentine', 36);
INSERT INTO `ludi_reponses` VALUES (272, 'Angleterre', 36);
INSERT INTO `ludi_reponses` VALUES (273, 'Chili', 36);
INSERT INTO `ludi_reponses` VALUES (274, 'Pérou', 36);
INSERT INTO `ludi_reponses` VALUES (275, 'Le professeur Binns', 3);
INSERT INTO `ludi_reponses` VALUES (276, 'Espèce de grosse brute repoussante', 37);
INSERT INTO `ludi_reponses` VALUES (277, 'Espèce de grosse bestiole repoussante', 37);
INSERT INTO `ludi_reponses` VALUES (278, 'Espèce de grosse bête repoussante', 37);
INSERT INTO `ludi_reponses` VALUES (279, 'Espèce de gros monstre repoussant', 37);
INSERT INTO `ludi_reponses` VALUES (280, 'Bartiméus Croupton Jr', 38);
INSERT INTO `ludi_reponses` VALUES (281, 'Igor Karkaroff', 38);
INSERT INTO `ludi_reponses` VALUES (282, 'Ludo Verpey', 38);
INSERT INTO `ludi_reponses` VALUES (283, 'Bellatrix Lestrange', 38);
INSERT INTO `ludi_reponses` VALUES (284, 'Remus Lupin', 38);
INSERT INTO `ludi_reponses` VALUES (285, 'Peter Pettigrow', 38);
INSERT INTO `ludi_reponses` VALUES (286, 'Sirius Black', 38);
INSERT INTO `ludi_reponses` VALUES (287, 'Graham Pritchard', 39);
INSERT INTO `ludi_reponses` VALUES (288, 'Stubby Boardman', 39);
INSERT INTO `ludi_reponses` VALUES (289, 'Augustus Pye', 39);
INSERT INTO `ludi_reponses` VALUES (290, 'Anthony Goldstein', 39);
INSERT INTO `ludi_reponses` VALUES (291, 'Gwenog Jones', 40);
INSERT INTO `ludi_reponses` VALUES (292, 'Dorcas Meadowes', 40);
INSERT INTO `ludi_reponses` VALUES (293, 'Araminta Meliflua', 40);
INSERT INTO `ludi_reponses` VALUES (294, 'Doris Purkiss', 40);
INSERT INTO `ludi_reponses` VALUES (295, 'Alastor Maugrey', 40);
INSERT INTO `ludi_reponses` VALUES (296, 'Charity Burbage', 40);
INSERT INTO `ludi_reponses` VALUES (297, 'Antioche Peverell', 39);
INSERT INTO `ludi_reponses` VALUES (298, 'Cadmus Peverell', 39);
INSERT INTO `ludi_reponses` VALUES (299, 'Ignotus Peverell', 39);
INSERT INTO `ludi_reponses` VALUES (300, 'Pour le plus grand bien', 41);
INSERT INTO `ludi_reponses` VALUES (301, 'La magie est puissance', 41);
INSERT INTO `ludi_reponses` VALUES (302, 'Les ténèbres sont puissance', 41);
INSERT INTO `ludi_reponses` VALUES (303, 'Le mal est puissance', 41);
INSERT INTO `ludi_reponses` VALUES (304, 'Pour le plus grand mal', 41);
INSERT INTO `ludi_reponses` VALUES (305, 'Une coupe', 42);
INSERT INTO `ludi_reponses` VALUES (306, 'Un vase', 42);
INSERT INTO `ludi_reponses` VALUES (307, 'Un diadème', 42);
INSERT INTO `ludi_reponses` VALUES (308, 'Une baguette', 42);
INSERT INTO `ludi_reponses` VALUES (309, 'Une médaille', 42);
INSERT INTO `ludi_reponses` VALUES (310, 'Une broche', 42);
INSERT INTO `ludi_reponses` VALUES (311, 'C\'est un cracmol', 43);
INSERT INTO `ludi_reponses` VALUES (312, 'C\'est un Animagus', 43);
INSERT INTO `ludi_reponses` VALUES (313, 'C\'est un Mangemort ', 43);
INSERT INTO `ludi_reponses` VALUES (314, 'C\'est un Détraquer', 43);
INSERT INTO `ludi_reponses` VALUES (315, 'Front de Libération des Elfes de Maison', 44);
INSERT INTO `ludi_reponses` VALUES (316, 'Association d\'Aide et de Secours aux Elfes', 44);
INSERT INTO `ludi_reponses` VALUES (317, 'Société d\'Aide à la Libération des Elfes ', 44);
INSERT INTO `ludi_reponses` VALUES (318, 'Comité de Libération des Elfes de Maison ', 44);
INSERT INTO `ludi_reponses` VALUES (319, 'Société d\'Aide à la Libération des Elfes de Maison', 44);
INSERT INTO `ludi_reponses` VALUES (320, 'Société pour la Libération des Elfes de Maison', 44);
INSERT INTO `ludi_reponses` VALUES (321, 'Rita Skeeter', 45);
INSERT INTO `ludi_reponses` VALUES (322, 'Rika Sleeter', 45);
INSERT INTO `ludi_reponses` VALUES (323, 'Rita Sleeter', 45);
INSERT INTO `ludi_reponses` VALUES (324, 'Rika Skeeter', 45);
INSERT INTO `ludi_reponses` VALUES (325, 'Le Baron Sanglant', 1);
INSERT INTO `ludi_reponses` VALUES (326, 'Un cheval', 4);
INSERT INTO `ludi_reponses` VALUES (327, ' Boutefeu chinois', 46);
INSERT INTO `ludi_reponses` VALUES (328, ' Magyar à pointes', 46);
INSERT INTO `ludi_reponses` VALUES (329, ' Vert gallois ', 46);
INSERT INTO `ludi_reponses` VALUES (330, ' Suédois à museau court ', 46);
INSERT INTO `ludi_reponses` VALUES (331, 'Une poire', 47);
INSERT INTO `ludi_reponses` VALUES (332, 'Une pomme', 47);
INSERT INTO `ludi_reponses` VALUES (333, 'Une orange', 47);
INSERT INTO `ludi_reponses` VALUES (334, 'Une banane', 47);
INSERT INTO `ludi_reponses` VALUES (335, 'Un citron', 47);
INSERT INTO `ludi_reponses` VALUES (336, 'Une pêche', 47);

-- ----------------------------
-- Table structure for ludi_stats
-- ----------------------------
DROP TABLE IF EXISTS `ludi_stats`;
CREATE TABLE `ludi_stats`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `games` int(255) NULL DEFAULT NULL,
  `win` int(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of ludi_stats
-- ----------------------------
INSERT INTO `ludi_stats` VALUES (1, 34, 18);

SET FOREIGN_KEY_CHECKS = 1;
